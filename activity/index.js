// S22 activity

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];
console.log(registeredUsers)
let friendsList = [];

// PART 1 - register user

function register(newUser){
    let userExist = registeredUsers.includes(newUser);
    if(userExist){
        alert("Registration failed. Username already exists!");
    } else {
        registeredUsers.push(newUser);
        alert("Thank you for registering!");
    }
};

// PART 2 - add user to friends list

function addToFriendsList(newUser){
    let userExist = registeredUsers.includes(newUser);
    if(userExist){
        friendsList.push(newUser);
        alert("You have added " + newUser + " as a friend!");
    } else {
        alert("User not found.");
    }
};


// PART 3 = display the friends list


function showFriends(){
    if(friendsList.length > 0){
       friendsList.forEach(friend => {
        console.log(friend);
       })
    } else {
        alert("You currently have 0 friends. Add one first.");
    }
};

// PART 4 - show the total number of friends

function showTotalFriends(){
    if(friendsList.length == 0){
        alert("You currently have 0 friends. Add one first.");
    } else {
        alert("You currently have " + friendsList.length + " friends.");
    }
};

// PART 5 - delete the last added friend

function deleteFriend(){
    if(friendsList.length == 0){
        alert("You currently have 0 friends. Add one first.");
    } else {
        friendsList.pop();
    }
}

// STRETCH GOAL - delete a specific friend

function deleteSpecificFriend(friend){
    if(friendsList.length > 0){
        let friendsIndex = friendsList.indexOf(friend);
        friendsList.splice(friendsIndex, 1)
    } else {
        alert("You currently have 0 friends. Add one first.");
    }
};




